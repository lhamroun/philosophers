# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/10/28 12:43:30 by lyhamrou          #+#    #+#              #
#    Updated: 2021/12/15 19:07:36 by lyhamrou         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = philo
FLAGS = -Wall -Wextra -Werror
HEADER_PATH = includes/
HEADER_NAME = philo.h
HEADER = $(addprefix $(HEADER_PATH), $(HEADER_NAME))
INCLUDE_HEADER = -I ./$(HEADER_PATH)

SRC_PATH = ./srcs/
SRC_NAME = debug.c main.c philo.c routine.c protected_data.c time.c actions.c ft_memset.c ft_strdel.c number_of_digit.c ft_strlen.c ft_atoi.c ft_atol.c
SRC = $(addprefix $(SRC_PATH), $(SRC_NAME))

OBJ_PATH = .obj/
OBJ_NAME = $(SRC_NAME:.c=.o)
OBJ = $(addprefix $(OBJ_PATH), $(OBJ_NAME))

all: $(NAME)

$(NAME): $(OBJ_PATH) $(OBJ)
	gcc $(FLAGS) -o $(NAME) $(OBJ) $(INCLUDE_HEADER) -pthread

$(OBJ_PATH):
	mkdir -p $(OBJ_PATH)

$(OBJ_PATH)%.o: $(SRC_PATH)%.c $(HEADER)
	gcc -c $(INCLUDE_HEADER) -o $@ -c $< -pthread

clean:
	$(RM) -rf $(OBJ_PATH)

fclean: clean
	$(RM) -rf $(NAME)

re: fclean all

.PHONY: all clean fclean re
