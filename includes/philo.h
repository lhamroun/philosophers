/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/28 12:46:06 by lyhamrou          #+#    #+#             */
/*   Updated: 2021/12/15 18:11:18 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_H
#define PHILO_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>
#include <errno.h>

// nb maximum of threads
#define MAX_PHILO 200
// time to wait inside routine loop
#define WAITING_VALUE 100
// Fork statement
#define NOT_TAKEN False
#define TAKEN True
// side of fork to take
#define RIGHT False
#define LEFT True

typedef enum		e_bool
{
	False = 0,
	True
}					t_bool;

// Philosopher state
typedef enum		e_state
{
	DEAD = 0,
	READY_TO_EAT,
	EATING,
	SLEEPING,
	THINKING
}					t_state;

typedef enum		e_set_behaviour
{
	TO_WAIT = 0,
	TO_SET,
	TO_PRINT
}					t_set_behaviour;

typedef struct		s_protected_data
{
	pthread_mutex_t	mutex;
	unsigned long	value;
}					t_protected_data;

typedef struct		s_fork
{
	// holder_index.value = 0 if not hold and philo->index hold
	t_protected_data		holder_index;
	t_protected_data		fork;
}					t_fork;

// easier to understand fork is protected data
typedef t_bool				t_fork_state;

// input parameters
typedef struct		s_param
{
	unsigned int	nb_philo; // maximum MAX_PHILO
	unsigned long	time_to_die;
	unsigned long	time_to_eat;
	unsigned long	time_to_sleep;
	unsigned long	nb_eat_max;
}					t_param;

// Philosopher main structure
typedef struct		s_philosopher
{
	unsigned int		index;
	t_set_behaviour		set_behaviour;
	t_state				state;
	unsigned long		nb_eat_time;
	unsigned long		timestamp; // timestamp (in ms) since beginning
	unsigned long		last_meal; // last timestamp (in ms) philosopher eat
	struct timeval		start_action_timestamp;
	t_fork				*fork_left;
	t_fork				*fork_right;
	pthread_mutex_t		*write_mutex; // mutex for print_philos_action()
	pthread_mutex_t		*take_fork_mutex;
	t_protected_data	*sync_mutex;// mutex use for synchronisation
	pthread_t			th;
}					t_philosopher;

// global variable point to allocated data --> singelton
typedef struct		s_env
{
	t_protected_data	*is_dead; // si j'ai un pb de concurrence je malloc ca?
	unsigned long		initial_time;// initial timestamp in ms
	t_param				param;
	t_philosopher		*philos; // @ of philo can be use at the end to free all
}					t_env;

/******************************************************************************
**		main functions                                                       **
******************************************************************************/
int					philosophers(void);
void				*philo_routine(void *param);
void				print_philos_action(t_philosopher *philo);
void				set_philos_action(t_philosopher *philo);
t_bool				check_if_action_is_finish(t_philosopher *philo);
void				print_tab(t_philosopher *philo);

/******************************************************************************
**		action functions                                                     **
******************************************************************************/
void				loop_action(t_philosopher *philo);
void				print_sleep_action(t_philosopher *philo);
void				print_eat_action(t_philosopher *philo);
void				print_think_action(t_philosopher *philo);
void				print_take_fork_action(t_philosopher *philo);
void				set_sleep_action(t_philosopher *philo);
void				set_eat_action(t_philosopher *philo);
void				set_think_action(t_philosopher *philo);
void				set_take_fork_action(t_philosopher *philo);
t_bool				check_if_can_eat(t_philosopher *philo);
t_bool				check_end_simulation(t_philosopher *philo);

/******************************************************************************
**		time functions                                                       **
******************************************************************************/
// calculate difference between actual timestamp (in ms) and diff
unsigned long		get_time_diff(unsigned long diff);
unsigned long		get_time_now(void);

/******************************************************************************
**		non classable                                                        **
******************************************************************************/
int					check_if_die(t_philosopher *philo);
t_env				*env_provider(void);

/******************************************************************************
**		data protection functions                                            **
******************************************************************************/
void				write_protected_data(t_protected_data *data, unsigned long val);
unsigned long		read_protected_data(t_protected_data *data);

/******************************************************************************
**		Utils                                                                **
******************************************************************************/
void				*ft_memset(void *s, int c, size_t n);
void				ft_strdel(char **str);
size_t				ft_strlen(const char *str);
int					ft_atoi(const char *str);
long long			ft_atol(const char *str);

/******************************************************************************
**		debug functions                                                      **
******************************************************************************/
void				print_env(t_env *env);
void				print_philo(t_philosopher *philo);
void				print_philos(t_philosopher *philo);
void				print_forks(void);
void				print_forks_locker(void);
size_t				number_of_digit(long nb);

#endif
