/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   protected_data.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/18 14:49:19 by lyhamrou          #+#    #+#             */
/*   Updated: 2021/11/18 14:49:54 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

unsigned long	read_protected_data(t_protected_data *data)
{
	unsigned long	ret;

	pthread_mutex_lock(&data->mutex);
	ret = data->value;
	pthread_mutex_unlock(&data->mutex);
	return (ret);
}

void	write_protected_data(t_protected_data *data, unsigned long value)
{
	pthread_mutex_lock(&data->mutex);
	data->value = value;
	pthread_mutex_unlock(&data->mutex);
}
