/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   routine.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/09 19:12:25 by lyhamrou          #+#    #+#             */
/*   Updated: 2021/12/15 18:46:15 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	reset_fork(t_philosopher *philo)
{
	if (philo->state == EATING)
	{
		pthread_mutex_unlock(&philo->fork_left->fork.mutex);
		pthread_mutex_unlock(&philo->fork_left->holder_index.mutex);
		pthread_mutex_unlock(&philo->fork_right->fork.mutex);
		pthread_mutex_unlock(&philo->fork_right->holder_index.mutex);
		pthread_mutex_lock(philo->take_fork_mutex);
		write_protected_data(&philo->fork_left->fork, NOT_TAKEN);
		write_protected_data(&philo->fork_left->holder_index, 0);
		write_protected_data(&philo->fork_right->fork, NOT_TAKEN);
		write_protected_data(&philo->fork_right->holder_index, 0);
		pthread_mutex_unlock(philo->take_fork_mutex);
	}
}

t_bool	check_if_dead(t_philosopher *philo)
{
	t_env	*env = env_provider();

	if (read_protected_data(env->is_dead) != False)
		return (True);
	if (get_time_now() - (philo->last_meal + env->initial_time) >= env->param.time_to_die)
	{
		philo->last_meal = get_time_now() - env->initial_time;
		philo->state = DEAD;
		write_protected_data(env->is_dead, (unsigned long)philo->index);
		return (True);
	}
	return (False);
}

t_bool	check_end_simulation(t_philosopher *philo)
{
	if (philo->nb_eat_time >= env_provider()->param.nb_eat_max)
	{
		reset_fork(philo);
		return (True);
	}
	if (check_if_dead(philo) != False)
	{
		reset_fork(philo);
		return (True);
	}
	return (False);
}

t_bool	check_if_action_is_finish(t_philosopher *philo)
{
	t_env			*env = env_provider();
	unsigned long	limit_time = 0;

	if (philo->state == SLEEPING)
		limit_time = env->param.time_to_sleep;
	else if (philo->state == EATING)
		limit_time = env->param.time_to_eat;
	if (get_time_now() - (philo->timestamp + env->initial_time) >= limit_time)
	{
		reset_fork(philo);
		return (True);
	}
	return (False);
}

void	loop_action(t_philosopher *philo)
{
	if (philo->state == READY_TO_EAT)
	{
		if (check_if_can_eat(philo) == True)
			philo->set_behaviour = TO_SET;
	}
	else if (philo->state == EATING || philo->state == SLEEPING)
	{
		if (check_if_action_is_finish(philo) == True)
		{
			philo->state = philo->state == EATING ? SLEEPING : THINKING;
			philo->set_behaviour = TO_SET;
		}
	}
}

void	set_philos_action(t_philosopher *philo)
{
	philo->set_behaviour = TO_PRINT;
}

void	print_philos_action(t_philosopher *philo)
{
	if (philo->state == READY_TO_EAT)
		print_take_fork_action(philo);
	else if (philo->state == EATING)
		print_eat_action(philo);
	else if (philo->state == SLEEPING)
		print_sleep_action(philo);
	else if (philo->state == THINKING)
		print_think_action(philo);
}

void	*philo_routine(void *param)
{
	t_philosopher	*philo;
	t_env			*env = env_provider();

	philo = (t_philosopher *)param;
	read_protected_data(philo->sync_mutex);
	while (check_end_simulation(philo) == False)
	{
		if (philo->set_behaviour == TO_SET)
			set_philos_action(philo);
		else if (philo->set_behaviour == TO_PRINT)
			print_philos_action(philo);
		else if (philo->set_behaviour == TO_WAIT)
			loop_action(philo);
		usleep(WAITING_VALUE);
	}
//	if (1) // quand je fais ca j'ai pas de probleme de destroy
	if (philo->state == EATING || philo->state == DEAD)
	{
		pthread_mutex_unlock(&philo->fork_left->fork.mutex);
		pthread_mutex_unlock(&philo->fork_left->holder_index.mutex);
		if (env_provider()->param.nb_philo > 1)
		{
			pthread_mutex_unlock(&philo->fork_right->fork.mutex);
			pthread_mutex_unlock(&philo->fork_right->holder_index.mutex);
		}
	}
	return (NULL);
}
