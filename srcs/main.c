/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/28 12:47:54 by lyhamrou          #+#    #+#             */
/*   Updated: 2021/12/09 14:17:48 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

t_env	*env_provider(void)
{
	static t_env	*env = NULL;

	if (!env)
	{
		if (!(env = (t_env *)malloc(sizeof(t_env))))
			return (NULL);
		ft_memset(env, 0, sizeof(t_env));
	}
	return (env);
}

void	print_error_message(int error)
{
	(void)error;
	printf("Error message function\n");
}

int		check_parameters(int ac, char **av)
{
	if (ac != 5 && ac != 6)
		return (6);
	for (int i = 1; i < ac; i++)
	{
		if (i == 1 && ft_atoi(av[i]) > MAX_PHILO)
			return (1);
		if (ft_atoi(av[i]) < 1 || (long long)ft_atoi(av[i]) != ft_atol(av[i]))
			return (i);
		if (number_of_digit(ft_atol(av[i])) + (av[i][0] == '+') != ft_strlen(av[i]))
			return (i + 100);
	}
	return 0;
}

int			init_env(int ac, char **av)
{
	t_env	*env = env_provider();

	env->param.nb_philo = (unsigned int)ft_atoi(av[1]);
	env->param.time_to_die = ft_atoi(av[2]);
	env->param.time_to_eat = ft_atoi(av[3]);
	env->param.time_to_sleep = ft_atoi(av[4]);
	if (ac == 6)
		env->param.nb_eat_max = ft_atoi(av[5]);
	else
		env->param.nb_eat_max = -1;
	env->is_dead = (t_protected_data *)malloc(sizeof(t_protected_data));
	if (!env->is_dead)
		return (1);
	ft_memset(env->is_dead, 0, sizeof(t_protected_data));
	if (pthread_mutex_init(&env->is_dead->mutex, NULL))
		return (2);
	return (0);
}

int		main(int ac, char **av)
{
	int		error = 0;
	t_env	*env = env_provider();

	if (check_parameters(ac, av))
	{
		ft_strdel((char **)&env);
		printf("usage: ./philo [number_of_philosophers] [time_to_die] [time_to_eat] [time_to_sleep] [[number_of_times_each_philosopher_must_eat]]\n");
		return (EXIT_SUCCESS);
	}
	if (init_env(ac, av))
	{
		ft_strdel((char **)&env->is_dead);
		ft_strdel((char **)&env);
		return (EXIT_FAILURE);
	}
	error = philosophers();
	if (error)
		print_error_message(error);
	ft_strdel((char **)&env->is_dead);
	ft_strdel((char **)&env);
	return (EXIT_SUCCESS);
}
