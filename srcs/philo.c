/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/29 17:45:40 by lyhamrou          #+#    #+#             */
/*   Updated: 2021/12/15 19:36:31 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void			init_initial_time(void)
{
	t_env			*env = env_provider();

	env->initial_time = get_time_now();
}

int				init_philo_mutex(unsigned int nb_philo, t_philosopher **address_philo)
{
	t_philosopher	*philo = *address_philo;

	philo[0].take_fork_mutex = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t));
	philo[0].write_mutex = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t));
	philo[0].sync_mutex = (t_protected_data *)malloc(sizeof(t_protected_data));
	philo[0].sync_mutex->value = 0;
	if (!philo[0].write_mutex || !philo[0].take_fork_mutex || !philo[0].sync_mutex)
		return (1);
	if (pthread_mutex_init(philo[0].write_mutex, NULL))
		return (2);
	if (pthread_mutex_init(philo[0].take_fork_mutex, NULL))
		return (3);
	if (pthread_mutex_init(&philo[0].sync_mutex->mutex, NULL))
		return (4);
	for (unsigned int i = 0; i < nb_philo; i++)
	{
		philo[i].write_mutex = philo[0].write_mutex;
		philo[i].take_fork_mutex = philo[0].take_fork_mutex;
		philo[i].sync_mutex = philo[0].sync_mutex;
	}
	return (0);
}

t_philosopher	*init_philosophers(int nb_philo)
{
	t_philosopher	*philo = NULL;
	t_env			*env = env_provider();

	philo = (t_philosopher *)malloc(sizeof(t_philosopher) * nb_philo);
	if (!philo)
		return NULL;
	ft_memset(philo, 0, sizeof(t_philosopher) * nb_philo);
	if (init_philo_mutex(nb_philo, &philo))
		return (NULL);
	for (int i = 0; i < nb_philo; i++)
	{
		philo[i].index = i + 1;
		philo[i].set_behaviour = TO_WAIT;
		philo[i].state = READY_TO_EAT;
		philo[i].nb_eat_time = 0;
		philo[i].timestamp = 0;
		philo[i].last_meal = 0;
		philo[i].fork_left = NULL;
		philo[i].fork_right = NULL;
	}
	env->philos = philo;
	return (philo);
}

int		init_forks(t_philosopher **address_philo, unsigned int nb_philo)
{
	int				right_fork_index = 0;
	t_philosopher	*philo = *address_philo;

	for (int i = 0; i < nb_philo; i++)
	{
		philo[i].fork_left = (t_fork *)malloc(sizeof(t_fork));
		if (!philo[i].fork_left)
			return (2);
		ft_memset(philo[i].fork_left, 0, sizeof(t_fork));
		if (pthread_mutex_init(&philo[i].fork_left->fork.mutex, NULL))
			return (3);
		if (pthread_mutex_init(&philo[i].fork_left->holder_index.mutex, NULL))
			return (4);
		philo[i].fork_left->fork.value = NOT_TAKEN;
		philo[i].fork_left->holder_index.value = 0;
	}
	if (nb_philo == 1)
		return (0);
	for (int i = 0; i < nb_philo; i++)
	{
		right_fork_index = i + 1;
		if (right_fork_index == nb_philo)
			right_fork_index = 0;
		philo[i].fork_right = philo[right_fork_index].fork_left;
	}
	return (0);
}

void	destroy_philos_mutex(t_philosopher *philo)
{
	ft_strdel((char **)&philo->write_mutex);
	ft_strdel((char **)&philo->take_fork_mutex);
	ft_strdel((char **)&philo->sync_mutex);
}

int		destroy_all_forks_mutex(unsigned int nb_philo, t_philosopher *philo)
{
	int		ret = 0;

	if (pthread_mutex_destroy(philo->write_mutex))
		return (-1);
	if (pthread_mutex_destroy(&philo->sync_mutex->mutex))
		return (-2);
	if (pthread_mutex_destroy(philo->take_fork_mutex))
		return (-3);
	for (unsigned int j = 0; j < nb_philo; j++)
	{
		ret += pthread_mutex_destroy(&philo[j].fork_left->fork.mutex);
		ret += pthread_mutex_destroy(&philo[j].fork_left->holder_index.mutex);
		if (ret)
		{
			printf("destroy philo %u - error : %d\n", philo[j].index, ret);
			return (j + 1);
		}
	}
	return (0);
}

int		desallocated_and_destroy_memory(t_env *env)
{
	t_philosopher	*philo = env->philos;

	if (destroy_all_forks_mutex(env->param.nb_philo, philo))
		return (1);
	if (pthread_mutex_destroy(&env->is_dead->mutex))
		return (2);
	for (unsigned int j = 0; j < env->param.nb_philo; j++)
		ft_strdel((char **)&env->philos[j].fork_left);
	destroy_philos_mutex(philo);
	ft_strdel((char **)&env->philos);
	return (0);
}

int		philosophers(void)
{
	int				ret = 0;
	int				i = 0;
	t_philosopher	*philo;
	t_env			*env = env_provider();

	philo = init_philosophers(env->param.nb_philo);
	if (!philo)
		return (2);
	if (init_forks(&philo, env->param.nb_philo))
	{
		destroy_philos_mutex(philo);
		ft_strdel((char **)&env->philos);
		return (3);
	}
	pthread_mutex_lock(&philo->sync_mutex->mutex);
	// Threads creation
	for (i = 0; i < env->param.nb_philo && !ret; i++)
		ret += pthread_create(&philo[i].th, NULL, philo_routine, (void *)&philo[i]);
	if (ret)
	{
		//destroy_philos_mutex(philo);
		return (4);
	}
	// wait creation of all threads
	usleep(500000);
	// Set initial Timestamp
	init_initial_time();
	pthread_mutex_unlock(&philo->sync_mutex->mutex);
	// Wait for all Threads
	for (i = 0; i < env->param.nb_philo; i++)
		ret += pthread_join(philo[i].th, NULL);
	if (ret)
	{
		//destroy_philos_mutex(philo);
		return (5);
	}
	// print philosopher death
	if (env->is_dead->value != False)
		printf("%lu %lu died\n", env->philos[env->is_dead->value - 1].last_meal, env->is_dead->value);
	//destroy mutex & desallocate memory
	if (desallocated_and_destroy_memory(env))
		return (6);
	return (0);
}
