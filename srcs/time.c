#include "philo.h"

unsigned long	get_time_now(void)
{
	struct timeval	timestamp;

	gettimeofday(&timestamp, NULL);
	return ((unsigned long)timestamp.tv_sec * 1000 + (unsigned long)timestamp.tv_usec / 1000);
}

unsigned long	get_time_diff(unsigned long diff)
{
	struct timeval	timestamp;
	unsigned long	ret = 0;

	gettimeofday(&timestamp, NULL);
	ret = (unsigned long)timestamp.tv_sec * 1000 + (unsigned long)timestamp.tv_usec / 1000;
	//printf("get time diff diff = %lu\n", diff);
	//printf("get time diff ret =  %lu\n", ret);
	//printf("ret - diff =         %lu\n", ret - diff);
	return (ret - diff);
}

unsigned long	get_time_diff_init(unsigned long diff)
{
	struct timeval	timestamp;
	unsigned long	ret = 0;

	gettimeofday(&timestamp, NULL);
	ret = ((unsigned long)timestamp.tv_sec * 1000 + (unsigned long)timestamp.tv_usec / 1000);
	ret = ret - env_provider()->initial_time;
	//printf("get time diff diff = %lu\n", diff);
	//printf("get time diff ret =  %lu\n", ret);
	//printf("ret - diff =         %lu\n", ret - diff);
	return (ret - diff);
}
