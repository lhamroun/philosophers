#include "philo.h"

void	print_env(t_env *env)
{
	printf("- PRINT ENV ---------------------------------------------------\n");
	printf("nb_philo %u\n", env->param.nb_philo);
	printf("time_to_die %lu\n", env->param.time_to_die);
	printf("time_to_eat %lu\n", env->param.time_to_eat);
	printf("time_to_sleep %lu\n", env->param.time_to_sleep);
	if (env->param.nb_eat_max != -1)
		printf("nb_eat_max %lu\n", env->param.nb_eat_max);
	printf("---------------------------------------------------------------\n\n");
}

void	print_philos(t_philosopher *philo)
{
	t_env				*env = env_provider();
	t_state				stat;

	printf("- PRINT PHILOS ------------------------------------------------\n");
	printf("________________________________________\n");
	for (int i = 0; i < env->param.nb_philo; i++)
	{
		stat = philo[i].state;
		printf("	Philo number   %d\n", philo[i].index);
		printf("	bit set_action %d\n", philo[i].set_behaviour);
		printf("	number of eat  %lu\n", philo[i].nb_eat_time);
		printf("	state          %s\n",stat==SLEEPING?"sleep":stat==EATING?"eat":stat==THINKING?"think":stat==READY_TO_EAT?"Ready to eat":"other");
		printf("	timestamp      %lu\n", philo[i].timestamp);
		printf("	- - - - - - - - - - - - - - - - - -\n");
		printf("	sync mutex   %p\n", philo[i].sync_mutex);
		printf("	write mutex  %p\n", philo[i].write_mutex);
		printf("	fork left @  %p\n", philo[i].fork_left);
		if (philo[i].fork_right)
			printf("	fork right @ %p\n", philo[i].fork_right);
		printf("	fork left  takken by %lu\n", read_protected_data(&(philo[i].fork_left->holder_index)));
		if (philo[i].fork_right)
			printf("	fork right takken by %lu\n", read_protected_data(&(philo[i].fork_right->holder_index)));
		printf("	fork_left  state %s\n", read_protected_data(&(philo[i].fork_left->fork)) == TAKEN ? "TAKEN" : "NOT TAKEN");
		if (philo[i].fork_right)
			printf("	fork_right state %s\n", read_protected_data(&(philo[i].fork_right->fork)) == TAKEN ? "TAKEN" : "NOT TAKEN");
		printf("________________________________________\n");
		printf("\n");
	}
	printf("---------------------------------------------------------------\n\n");
}

void	print_philo(t_philosopher *philo)
{
	t_env		*env = env_provider();
	t_state		stat;

	printf("- PRINT PHILO -------------------------------------------------\n");
	printf("________________________________________\n");
	stat = philo->state;
	printf("	Philo number   %d\n", philo->index);
	printf("	bit set_action %d\n", philo->set_behaviour);
	printf("	number of eat  %lu\n", philo->nb_eat_time);
	printf("	state          %s\n",stat==SLEEPING?"sleep":stat==EATING?"eat":stat==THINKING?"think":stat==READY_TO_EAT?"Ready to eat":"other");
	printf("	timestamp      %lu\n", philo->timestamp);
	printf("	- - - - - - - - - - - - - - - - - -\n");
	printf("	fork left @  %p\n", philo->fork_left);
	if (philo->fork_right)
		printf("	fork right @ %p\n", philo->fork_right);
	printf("	fork left  takken by %lu\n", read_protected_data(&(philo->fork_left->holder_index)));
	if (philo->fork_right)
		printf("	fork right takken by %lu\n", read_protected_data(&(philo->fork_right->holder_index)));
	printf("	fork_left  state %s\n", read_protected_data(&(philo->fork_left->holder_index)) && read_protected_data(&(philo->fork_left->fork)) == TAKEN ? "TAKEN" : "NOT TAKEN");
	if (philo->fork_right)
		printf("	fork_right state %s\n", read_protected_data(&(philo->fork_right->holder_index)) && read_protected_data(&(philo->fork_right->fork)) == TAKEN ? "TAKEN" : "NOT TAKEN");
	printf("________________________________________\n");
	printf("---------------------------------------------------------------\n\n");
}

void	print_forks(void)
{
	t_env			*env = env_provider();
	t_philosopher	*philo = env->philos;

	printf("- PRINT FORKS -------------------------------------------------\n");
	for (unsigned int i = 0; i <env->param.nb_philo; i++)
	{
		printf("philo %u - ", philo[i].index);
		if (philo[i].fork_left->holder_index.value == philo[i].index)
		{
			printf("fork left %s\n", philo[i].fork_left->fork.value == TAKEN ? "TAKEN" : "NOT TAKEN");
			printf("          fork right  %s\n", philo[i].fork_right->fork.value == TAKEN ? "TAKEN" : "NOT TAKEN");
		}
		else
			printf("not taking fork\n");
		printf("\n");
	}
	printf("---------------------------------------------------------------\n\n");
}

void	print_forks_locker(void)
{
	t_env	*env = env_provider();
	t_philosopher	*philo = env->philos;

	printf("- PRINT FORKS_LOCKER ------------------------------------------\n");
	for (unsigned int i = 0; i <env->param.nb_philo; i++)
	{
		printf("philo index %u\n", philo[i].index);
		//printf("forks_locker @ %p\n", philo[i].forks_locker);
		printf("\n- - - - - - -\n");
	}
	printf("---------------------------------------------------------------\n\n");
}
