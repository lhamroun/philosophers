/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   actions.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/18 15:19:10 by lyhamrou          #+#    #+#             */
/*   Updated: 2021/12/15 19:08:35 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	print_philo_action(t_philosopher *philo)
{
	unsigned long	time = env_provider()->initial_time;
	char			print_tab[5][17] = {"died",
								"has taken a fork",
								"is eating",
								"is sleeping",
								"is thinking"};

	pthread_mutex_lock(philo->write_mutex);
	printf("%lu %u %s\n", get_time_diff(time), philo->index, print_tab[philo->state]);
	pthread_mutex_unlock(philo->write_mutex);
}

void	print_take_fork_action(t_philosopher *philo)
{
	philo->timestamp = get_time_diff(env_provider()->initial_time);
	gettimeofday(&philo->start_action_timestamp, NULL);
	print_philo_action(philo);
	print_philo_action(philo);
	philo->state = EATING;
	philo->set_behaviour = TO_SET;
}

void	print_eat_action(t_philosopher *philo)
{
	philo->timestamp = get_time_diff(env_provider()->initial_time);
	gettimeofday(&philo->start_action_timestamp, NULL);
	philo->last_meal = philo->timestamp;
	print_philo_action(philo);
	philo->nb_eat_time++;
	philo->set_behaviour = TO_WAIT;
}

void	print_sleep_action(t_philosopher *philo)
{
	philo->timestamp = get_time_diff(env_provider()->initial_time);
	gettimeofday(&philo->start_action_timestamp, NULL);
	print_philo_action(philo);
	philo->set_behaviour = TO_WAIT;
}

void	print_think_action(t_philosopher *philo)
{
	philo->timestamp = get_time_diff(env_provider()->initial_time);
	gettimeofday(&philo->start_action_timestamp, NULL);
	print_philo_action(philo);
	philo->state = READY_TO_EAT;
	philo->set_behaviour = TO_WAIT;
}

t_bool	check_if_can_eat(t_philosopher *philo)
{
	t_fork			*fork1;
	t_fork			*fork2;

	fork1 = philo->index % 2 ? philo->fork_right : philo->fork_left;
	fork2 = philo->index % 2 ? philo->fork_left : philo->fork_right;
	if (env_provider()->param.nb_philo > 1)
	{
		pthread_mutex_lock(philo->take_fork_mutex);
		if (read_protected_data(&fork1->holder_index) == 0
			&& read_protected_data(&fork2->holder_index) == 0)
		{
			if (read_protected_data(&fork1->fork) == NOT_TAKEN
				&& read_protected_data(&fork2->fork) == NOT_TAKEN)
			{
				write_protected_data(&fork1->holder_index, philo->index);
				write_protected_data(&fork2->holder_index, philo->index);
				write_protected_data(&fork1->fork, TAKEN);
				write_protected_data(&fork2->fork, TAKEN);
				pthread_mutex_lock(&fork1->fork.mutex);
			//	pthread_mutex_lock(&fork1->holder_index.mutex);
				pthread_mutex_lock(&fork2->fork.mutex);
			//	pthread_mutex_lock(&fork2->holder_index.mutex);
				pthread_mutex_unlock(philo->take_fork_mutex);
				philo->state = READY_TO_EAT;
				philo->set_behaviour = TO_SET;
				return (True);
			}
		}
	}
	pthread_mutex_unlock(philo->take_fork_mutex);
	return (False);
}
